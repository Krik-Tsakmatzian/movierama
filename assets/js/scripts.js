/*jslint browser: true*/

/*global
    apikey, basicUrl, inTheaters, movies, reachLimit, header, loadDetails, loadMovies, $, _, window
*/

/*property
    Deferred, abridged_cast, abridged_directors, ajax, append, audience_score,
    className, critic, css, data, dataType, date, detailed, done, empty,
    forEach, genres, height, hide, id, indexOf, left, length, map, margin,
    movies, name, offset, on, pop, position, posters, prepend, promise, quote,
    ratings, ready, remove, resolve, reviews, right, runtime, scroll,
    scrollTop, show, slice, slideToggle, split, stopPropagation, success,
    synopsis, target, then, title, toString, top, total, type, url, val, when,
    year
*/

var apikey = "qtqep7qydngcc7grk4r4hyd9";
var basicUrl = "http://api.rottentomatoes.com/api/public/v1.0/";
var inTheaters = "lists/movies/in_theaters.json";
var movies = "movies.json";
var reachLimit = false;
var header;



function loadMovies(pageNumber, linkData, searchKeyword) {
    "use strict";

    $('a#inifiniteLoader').show('fast');
    $.ajax({
        url: basicUrl + linkData,
        type: "GET",
        dataType: "jsonp",
        data: "apikey=" + apikey + "&page_limit=10&&q=" + searchKeyword + "&page=" + pageNumber,
        success: function (data) {
            if (header !== data.total && !searchKeyword) {
                header = data.total;
                $(".header").remove();
                $("#header .container").prepend('<h1 class="header">' + header + ' movies in theaters this week</h1>');
            } else if (header !== data.total && searchKeyword) {
                header = data.total;
                $(".header").remove();
                $("#header .container").prepend('<h1 class="header">Found ' + header + ' movies</h1>');
            }
            $('a#inifiniteLoader').hide('1000');
            var countDivs = $("#main > div").length;
            if (countDivs < data.total) {
                data.movies.forEach(function (movie) {
                    movie.abridged_cast = movie.abridged_cast.slice(0, 3).map(function (cast, index) {
                        if (index < 3) {
                            return cast.name;
                        }
                    });
                    $("#main").append('<div class="row" id=' + movie.id + '></div>');
                    $("#" + movie.id).append('<div class="col-md-1 ' + movie.id + '"></div><div class="col-md-11 ' + movie.id + '"></div>');
                    $("#" + movie.id + " .col-md-1").append('<img class=' + movie.id + ' src="' + movie.posters.detailed + '" alt=""/>');
                    $("#" + movie.id + " .col-md-11").append('<h2 class=' + movie.id + '>' + movie.title + '</h2><h4 class=' + movie.id + '>' + movie.year + ' - ' + movie.runtime + ' mins  - ' + movie.ratings.audience_score + '/100</h4>');
                    $("#" + movie.id + " .col-md-11").append('<h3 class=' + movie.id + '>' + movie.abridged_cast + '</h4>');
                    $("#" + movie.id + " .col-md-11").append('<p class=' + movie.id + '>' + movie.synopsis + '</p>');
                });
            } else {
                reachLimit = true;
            }
        }
    });
}



function loadDetails(linkData) {
    "use strict";

    return $.ajax({
        url: basicUrl + linkData,
        type: "GET",
        dataType: "jsonp",
        data: "apikey=" + apikey
    });
}




$(document).ready(function () {
    "use strict";

    var count = 2;
    var typing;
    var stickyHeader = $('header').offset().top;


    function sticky_navigation() {
        var scroll_top = $(window).scrollTop();

        if (scroll_top > stickyHeader) {
            $('header').css({'position': 'fixed', 'top': 0, 'left': 0, 'right': 0, 'margin': '0 auto'});
        } else {
            $('header').css({'position': 'relative'});
        }
    }


    /******************** INITIALIZE IN THEATERS MOVIES *****************/
    loadMovies("1", inTheaters);
    /******************** END - INITIALIZE IN THEATERS MOVIES *****************/


    /******************** ALWAYS VISIBLE HEADER *****************/
    sticky_navigation();

    $(window).scroll(function () {
        sticky_navigation();
    });
    /******************** END - ALWAYS VISIBLE HEADER *****************/


    /******************** INFINITE SCROLL TRIGGER *****************/
    $(window).scroll(function () {
        if ($(window).scrollTop() === $(document).height() - $(window).height()) {
            var searchInput = $(".searchInput input").val();
            if (searchInput.length > 0) {
                if (reachLimit === false) {
                    loadMovies(count.toString(), movies, searchInput);
                }
            } else {
                if (reachLimit === false) {
                    loadMovies(count.toString(), inTheaters);
                }
            }
            count += 1;
        }
    });
    /******************** ENDE - INFINITE SCROLL TRIGGER *****************/


    /******************** SEARCH INPUT TRIGGER *****************/
    $(".searchInput input").on('input', function () {
        clearTimeout(typing);
        typing = setTimeout(function () {
            var usersInput = $(".searchInput input").val();
            if (usersInput.length > 0) {
                $("#main").empty();
                reachLimit = false;
                clearTimeout(typing);
                loadMovies("1", movies, usersInput);
                count = 2;
            } else {
                $("#main").empty();
                reachLimit = false;
                clearTimeout(typing);
                loadMovies("1", inTheaters);
                count = 2;
            }
        }, 500);
    });


    $(".searchInput input").on('keydown', function () {
        clearTimeout(typing);
    });
    /******************** END - SEARCH INPUT TRIGGER *****************/



    $('#main').on('click', ':not(.row)', function (e) {
        var movie = e.target.className;
        e.stopPropagation();
        function getDetails() {
            var deferred = new $.Deferred();

            var moviesDetails = loadDetails('movies/' + movie + '.json');
            var reviews = loadDetails('movies/' + movie + '/reviews.json');
            var similar = loadDetails('movies/' + movie + '/similar.json');

            $.when(moviesDetails, reviews, similar)
                .done(function (response1, response2, response3) {
                    deferred.resolve(response1, response2, response3);
                });

            return deferred.promise();
        }


        if (movie.indexOf("col") > -1 || movie.indexOf("more") > -1 || movie.indexOf("review") > -1) {
            movie = movie.split(" ").pop();
        }

        if ($('#' + movie + ' .more').length) {
            $('#' + movie + ' .more').slideToggle("slow");
        } else {


            getDetails().then(function (detailsResponse, reviewsResponse, similarResponse) {

                var director = _.map(detailsResponse[0].abridged_directors, function (director) {
                    return director.name;
                });


                $('#' + movie + ' .col-md-11').append('<div class="more ' + movie + '" style="display:none;"></div>');
                if (detailsResponse[0].genres.length > 0) {
                    $('#' + movie + ' .more').append('<h3 class="' + movie + '"><span class="' + movie + '">Movie genre: </span>' + detailsResponse[0].genres.toString() + '</h4>');
                }
                if (director.length > 0) {
                    $('#' + movie + ' .more').append('<h3 class="' + movie + '"><span class="' + movie + '">Director:</span> ' + director.toString() + '</h4>');
                }
                if (similarResponse[0].movies.length > 0) {
                    $('#' + movie + ' .more').append('<h3 class="' + movie + '"><span class="' + movie + '">Similar Movies</span></h4><ul class="' + movie + '"></ul>');
                    _.forEach(similarResponse[0].movies, function (similar) {
                        $('ul.' + movie).append('<li class="' + movie + '">' + similar.title + '</li>');
                    });
                }
                if (reviewsResponse[0].reviews.length > 0) {
                    $('#' + movie + ' .more').append('<div class="review ' + movie + '"><h3 class="' + movie + '">Reviews</h3><ul class="' + movie + '"></ul></div>');
                    _.forEach(reviewsResponse[0].reviews, function (review, index) {
                        if (index < 2) {
                            $('.review ul.' + movie).append('<li class="' + movie + '"><h4 class="' + movie + '">' + review.critic + ' | ' + review.date + '</h4><p class="' + movie + '">' + review.quote + '</p></li>');
                        }
                    });
                }
                $('#' + movie + ' .more').slideToggle("slow");

            });
        }

    });



});